package com.justmoreinnovation; /**
 * Created by Yevgeniy on 6/17/15.
 */
import java.util.Scanner;
import java.util.Random;


public class GuessingGame {
    private static final int RANGE_MIN = 0;
    private static final int RANGE_MAX = 10;

    public static void main(String[] args) {
        Random rand = new Random();

        Integer computerValue = rand.nextInt(RANGE_MAX - RANGE_MIN)+RANGE_MIN;
        Integer userGuess = null;
        int guesses = 0;

        Scanner reader = new Scanner(System.in);

        System.out.println("I am thinking of a number between " + RANGE_MIN + " and " + RANGE_MAX);


        do {
            System.out.print("Guess: ");
            guesses++;
            try{
                userGuess = reader.nextInt();

                if(userGuess > computerValue)
                {
                    System.out.println("The number I am thinking of is smaller than " + userGuess);
                }
                else if(userGuess < computerValue)
                {
                    System.out.println("The number I am thinking of is larger than " + userGuess);
                }

            }catch(Exception e)
            {

            }
        }while(!computerValue.equals(userGuess));

        System.out.print("Correct, I was thinking of  " + computerValue + " It only took you " + guesses + " tries.");
    }
}
